# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

## Descrição do projeto

&nbsp;&nbsp;Projeto focado na aplicação dos conhecimentos dentro e fora de sala para a criação de um jogo, Batalha Naval, para 2 jogadores. 

## Instruções de execução

&nbsp;&nbsp;De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```

## Observações

* Projeto feito no Code Blocks 64 bits, em um Windows 7 Home Premium;
* Testado em um Visual Studio Code de Windows 7;
* Por erros de entendimento de minha parte, o projeto foi feito em um diretório separado, por isso os arquivos estão com poucos commitds no diretório "forkado";
* [Link](https://gitlab.com/rhuanm/eps-1) para o diretório original do projeto;
* testado, porem com erros, no Ubuntu 16.04 (não tenho domínio suficente sobre o Sistema Operacional para corrigir tais erros);