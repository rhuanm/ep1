#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include "../inc/class.hpp"
#include "../inc/tirarLinhas.hpp"
#include "../inc/mapa.hpp"

using namespace std;

int main()
{
    fstream arquivo;
    string nomeArquivo = "../doc/map_1.txt.txt";//string arquivo
    string str, temporarioLinha, temporarioColuna;//strings universais
    string P1CTipo[6], P1CLoca[6], P1StringCanoa;//strings canoa player 1
    string P1STipo[4], P1SLoca[4], P1StringSubmarino;//strings submarino player 1
    string P1PATipo[2], P1PALoca[2], P1StringPortaA;//strings porta-aviao player 1

    string P2CTipo[6], P2CLoca[6], P2StringCanoa;//strings canoa player 2
    string P2STipo[4], P2SLoca[4], P2StringSubmarino;//strings submarino player 2
    string P2PATipo[2], P2PALoca[2], P2StringPortaA;//strings porta-aviao player 2

    vector <canoa*> ca1;//criar um vector da classe canoa para o player 1
    vector <submarino*> sub1;//criar um vector da classe submarino para o player 1
    vector <porta_avioes*> pa1;//criar um vector da classe porta_avioes para o player 1

    vector <canoa*> ca2;//criar um vector da classe canoa para o player 2
    vector <submarino*> sub2;//criar um vector da classe submarino para o player 2
    vector <porta_avioes*> pa2;//criar um vector da classe porta_avioes para o player 2

    char comando;//char para a leitura do fim do jogo
    int pos, tam, var1P1, var2P1, var1P2, var2P2, contLinha=0;//variaveis universais
    int P1CLinha[6], P1CColuna[6], P1contC=0, cano1=0;//variaveis canoa player 1
    int P1SLinha[4], P1SColuna[4], P1contS=0, subm1=0;//variaveis canoa player 1
    int P1PALinha[2], P1PAColuna[2], P1contPA=0, port1=0;//variaveis canoa player 1

    int P2CLinha[6], P2CColuna[6], P2contC=0, cano2=0;//variaveis canoa player 2
    int P2SLinha[4], P2SColuna[4], P2contS=0, subm2=0;//variaveis canoa player 2
    int P2PALinha[2], P2PAColuna[2], P2contPA=0, port2=0;//variaveis canoa player 2

    limparLinhas(nomeArquivo);//função que retira comentários e linhas nulas do códico e cria um novo arquivo

    arquivo.open("../doc/new_map_1.txt.txt", ios::in);//abre o arquiv txt sem os comentários e linhas nulas

    if(arquivo.is_open())
    {
        while(getline(arquivo,str))
        {
                if(contLinha<6)//separa as strings de dado refentes à canoa do player 1
                {
                    P1StringCanoa=str;

                    pos = P1StringCanoa.find(" ", 0);//procura a primeira ocorrencia de espaço na string
                    temporarioLinha = P1StringCanoa.substr(0, pos);//copia a string menos o espaço
                    P1CLinha[P1contC] =  atoi(temporarioLinha.c_str());//converte para inteiro

                    tam = pos+1;
                    pos = P1StringCanoa.find(" ", pos+1);
                    temporarioColuna = P1StringCanoa.substr(tam, pos-tam);
                    P1CColuna[P1contC] = atoi(temporarioColuna.c_str());

                    tam = pos+1;
                    pos = P1StringCanoa.find(" ", pos+1);
                    P1CTipo[P1contC] = P1StringCanoa.substr(tam, pos-tam);

                    tam = pos+1;
                    pos = P1StringCanoa.find(" ", pos+1);
                    P1CLoca[P1contC] = P1StringCanoa.substr(tam, pos-tam);

                    P1contC++;
                }

                else if(contLinha>=6 && contLinha<10)//separa as strings de dado refentes à subimarino do player 1
                {
                   P1StringSubmarino=str;

                    pos = P1StringSubmarino.find(" ", 0);
                    temporarioLinha = P1StringSubmarino.substr(0, pos);
                    P1SLinha[P1contS] =  atoi(temporarioLinha.c_str());

                    tam = pos+1;
                    pos = P1StringSubmarino.find(" ", pos+1);
                    temporarioColuna = P1StringSubmarino.substr(tam, pos-tam);
                    P1SColuna[P1contS] = atoi(temporarioColuna.c_str());

                    tam = pos+1;
                    pos = P1StringSubmarino.find(" ", pos+1);
                    P1STipo[P1contS] = P1StringSubmarino.substr(tam, pos-tam);

                    tam = pos+1;
                    pos = P1StringSubmarino.find(" ", pos+1);
                    P1SLoca[P1contS] = P1StringSubmarino.substr(tam, pos-tam);

                    P1contS++;
                }

                else if(contLinha>=10 && contLinha<12)//separa as strings de dado refentes à porta_avioes do player 1
                {
                    P1StringPortaA=str;

                    pos = P1StringPortaA.find(" ", 0);
                    temporarioLinha = P1StringPortaA.substr(0, pos);
                    P1PALinha[P1contPA] =  atoi(temporarioLinha.c_str());

                    tam = pos+1;
                    pos = P1StringPortaA.find(" ", pos+1);
                    temporarioColuna = P1StringPortaA.substr(tam, pos-tam);
                    P1PAColuna[P1contPA] = atoi(temporarioColuna.c_str());

                    tam = pos+1;
                    pos = P1StringPortaA.find(" ", pos+1);
                    P1PATipo[P1contPA] = P1StringPortaA.substr(tam, pos-tam);

                    tam = pos+1;
                    pos = P1StringPortaA.find(" ", pos+1);
                    P1PALoca[P1contPA] = P1StringPortaA.substr(tam, pos-tam);

                    P1contPA++;
                }

                else if(contLinha>=12 && contLinha<18)//separa as strings de dado refentes à canoa do player 2
                {
                    P2StringCanoa=str;

                    pos = P2StringCanoa.find(" ", 0);
                    temporarioLinha = P2StringCanoa.substr(0, pos);
                    P2CLinha[P2contC] = atoi(temporarioLinha.c_str());//converte para inteiro

                    tam = pos+1;
                    pos = P2StringCanoa.find(" ", pos+1);
                    temporarioColuna = P2StringCanoa.substr(tam, pos-tam);
                    P2CColuna[P2contC] = atoi(temporarioColuna.c_str());

                    tam = pos+1;
                    pos = P2StringCanoa.find(" ", pos+1);
                    P2CTipo[P2contC] = P2StringCanoa.substr(tam, pos-tam);

                    tam = pos+1;
                    pos = P2StringCanoa.find(" ", pos+1);
                    P2CLoca[P2contC] = P2StringCanoa.substr(tam, pos-tam);

                    P2contC++;
                }

                else if(contLinha>=18 && contLinha<22)//separa as strings de dado refentes à submarino do player 2
                {
                    P2StringSubmarino=str;

                    pos = P2StringSubmarino.find(" ", 0);
                    temporarioLinha = P2StringSubmarino.substr(0, pos);
                    P2SLinha[P2contS] =  atoi(temporarioLinha.c_str());

                    tam = pos+1;
                    pos = P2StringSubmarino.find(" ", pos+1);
                    temporarioColuna = P2StringSubmarino.substr(tam, pos-tam);
                    P2SColuna[P2contS] = atoi(temporarioColuna.c_str());

                    tam = pos+1;
                    pos = P2StringSubmarino.find(" ", pos+1);
                    P2STipo[P2contS] = P2StringSubmarino.substr(tam, pos-tam);

                    tam = pos+1;
                    pos = P2StringSubmarino.find(" ", pos+1);
                    P2SLoca[P2contS] = P2StringSubmarino.substr(tam, pos-tam);

                    P2contS++;
                }

                else if(contLinha>=22)//separa as strings de dado refentes à submarino do player 2
                {
                    P2StringPortaA=str;

                    pos = P2StringPortaA.find(" ", 0);
                    temporarioLinha = P2StringPortaA.substr(0, pos);
                    P2PALinha[P2contPA] =  atoi(temporarioLinha.c_str());

                    tam = pos+1;
                    pos = P2StringPortaA.find(" ", pos+1);
                    temporarioColuna = P2StringPortaA.substr(tam, pos-tam);
                    P2PAColuna[P2contPA] = atoi(temporarioColuna.c_str());

                    tam = pos+1;
                    pos = P2StringPortaA.find(" ", pos+1);
                    P2PATipo[P2contPA] = P2StringPortaA.substr(tam, pos-tam);

                    tam = pos+1;
                    pos = P2StringPortaA.find(" ", pos+1);
                    P2PALoca[P2contPA] = P2StringPortaA.substr(tam, pos-tam);

                    P2contPA++;

                }

            contLinha++;

        }
    }
    else
    {
        cout <<"nao foi possivel abrir o arquivo"<<endl;
    }

    canoa *p1c1=new canoa(P1CLinha[0],P1CColuna[0],P1CTipo[0],P1CLoca[0]);//cria um novo objeto na classe canoa para o player 1
    canoa *p1c2=new canoa(P1CLinha[1],P1CColuna[1],P1CTipo[1],P1CLoca[1]);
    canoa *p1c3=new canoa(P1CLinha[2],P1CColuna[2],P1CTipo[2],P1CLoca[2]);
    canoa *p1c4=new canoa(P1CLinha[3],P1CColuna[3],P1CTipo[3],P1CLoca[3]);
    canoa *p1c5=new canoa(P1CLinha[4],P1CColuna[4],P1CTipo[4],P1CLoca[4]);
    canoa *p1c6=new canoa(P1CLinha[5],P1CColuna[5],P1CTipo[5],P1CLoca[5]);

    submarino *p1s1=new submarino(P1SLinha[0],P1SColuna[0],P1STipo[0],P1SLoca[0]);//cria um novo objeto na classe submarino para o player 1
    submarino *p1s2=new submarino(P1SLinha[1],P1SColuna[1],P1STipo[1],P1SLoca[1]);
    submarino *p1s3=new submarino(P1SLinha[2],P1SColuna[2],P1STipo[2],P1SLoca[2]);
    submarino *p1s4=new submarino(P1SLinha[3],P1SColuna[3],P1STipo[3],P1SLoca[3]);

    porta_avioes *p1pa1=new porta_avioes(P1PALinha[0],P1PAColuna[0],P1PATipo[0],P1PALoca[0]);//cria um novo objeto na classe canoa para o player 1
    porta_avioes *p1pa2=new porta_avioes(P1PALinha[1],P1PAColuna[1],P1PATipo[1],P1PALoca[1]);

    canoa *p2c1=new canoa(P2CLinha[0],P2CColuna[0],P2CTipo[0],P2CLoca[0]);//cria um novo objeto na classe canoa para o player 2
    canoa *p2c2=new canoa(P2CLinha[1],P2CColuna[1],P2CTipo[1],P2CLoca[1]);
    canoa *p2c3=new canoa(P2CLinha[2],P2CColuna[2],P2CTipo[2],P2CLoca[2]);
    canoa *p2c4=new canoa(P2CLinha[3],P2CColuna[3],P2CTipo[3],P2CLoca[3]);
    canoa *p2c5=new canoa(P2CLinha[4],P2CColuna[4],P2CTipo[4],P2CLoca[4]);
    canoa *p2c6=new canoa(P2CLinha[5],P2CColuna[5],P2CTipo[5],P2CLoca[5]);

    submarino *p2s1=new submarino(P2SLinha[0],P2SColuna[0],P2STipo[0],P2SLoca[0]);//cria um novo objeto na classe submarino para o player 2
    submarino *p2s2=new submarino(P2SLinha[1],P2SColuna[1],P2STipo[1],P2SLoca[1]);
    submarino *p2s3=new submarino(P2SLinha[2],P2SColuna[2],P2STipo[2],P2SLoca[2]);
    submarino *p2s4=new submarino(P2SLinha[3],P2SColuna[3],P2STipo[3],P2SLoca[3]);

    porta_avioes *p2pa1=new porta_avioes(P2PALinha[0],P2PAColuna[0],P2PATipo[0],P2PALoca[0]);//cria um novo objeto na classe porta_avioes para o player 2
    porta_avioes *p2pa2=new porta_avioes(P2PALinha[1],P2PAColuna[1],P2PATipo[1],P2PALoca[1]);

    ca1.push_back(p1c1);//aloca no último elemento do vector o objeto
    ca1.push_back(p1c2);
    ca1.push_back(p1c3);
    ca1.push_back(p1c4);
    ca1.push_back(p1c5);
    ca1.push_back(p1c6);

    sub1.push_back(p1s1);
    sub1.push_back(p1s2);
    sub1.push_back(p1s3);
    sub1.push_back(p1s4);

    pa1.push_back(p1pa1);
    pa1.push_back(p1pa2);

    ca2.push_back(p2c1);
    ca2.push_back(p2c2);
    ca2.push_back(p2c3);
    ca2.push_back(p2c4);
    ca2.push_back(p2c5);
    ca2.push_back(p2c6);

    sub2.push_back(p2s1);
    sub2.push_back(p2s2);
    sub2.push_back(p2s3);
    sub2.push_back(p2s4);

    pa2.push_back(p2pa1);
    pa2.push_back(p2pa2);

    Limpar();//função para limpar lixos da matriz

    while(cano1<6)//posiciona os elementos canoa para o player 1
    {
        ca1[cano1]->posicionar_CanoaP1();
        cano1++;
    }

    while(subm1<4)//posiciona os elementos submarino para o player 1
    {
        sub1[subm1]->posicionar_SubmarinoP1();
        subm1++;
    }

    while(port1<2)//posiciona os elementos porta_avioes para o player 1
    {
        pa1[port1]->posicionar_PortaAvioesP1();
        port1++;
    }

    while(cano2<6)//posiciona os elementos canoa para o player 2
    {
        ca2[cano2]->posicionar_CanoaP2();
        cano2++;
    }

    while(subm2<4)//posiciona os elementos submarino para o player 2
    {
        sub2[subm2]->posicionar_SubmarinoP2();
        subm2++;
    }

    while(port2<2)//posiciona os elementos porta_avioes para o player 2
    {
        pa2[port2]->posicionar_PortaAvioesP2();
        port2++;
    }

    while(1)//laço de repetição para a execução do jogo
        {
                cout << "Player1 coloque localizacao: ";
                cin >> var1P1 >> var2P1;//valores que o Player 1 dá para atacar o Player 2
                if(AtaqueP2(var1P1,var2P1))//condição que determina se Player 1 acertou ou errou
                {
                    cout << "Acertou! :)" << endl;
                }

                else
                {
                    cout << "Errou!" << endl;
                }
                cout<<endl;
                cout<<"mapa player 2"<< endl;
                MostrarP2();//mostra na tela como está o mapa do player 2
                cout<<endl;
                cout << "Numero de navios do Player2: " << NumeroDeBarcosP2() << endl;//nostra na tela o número de barcos restantes para o player 2
                if(NumeroDeBarcosP2() == 0)//se não tiverem mais barcos o jogo acaba
                {
                    break;
                }

                cout << "Player2 coloque localizacao: ";
                cin >> var1P2 >> var2P2;//valores que o Player 2 dá para atacar o Player 1
                if(AtaqueP1(var1P2,var2P2))//condição que determina se Player 2 acertou ou errou
                {
                    cout << "Acertou! :)" << endl;
                }

                else
                {
                    cout << "Errou!" << endl;
                }
                cout<<endl;
                cout<<"mapa player 1"<< endl;
                MostrarP1();//mostra na tela como está o mapa do player 1
                cout<<endl;
                cout << "Numero de navios do Player1: " << NumeroDeBarcosP1() << endl;//nostra na tela o número de barcos restantes para o player 2
                if(NumeroDeBarcosP1() == 0)
                {
                    break;
                }
                cout << "Deseja acabar (s/n)? ";//caso o jogador queira acabar o jogo antes
                cin >> comando;

                if(comando == 's')
                {
                    break;
                }

        }
        cout << "Fim de Jogo!" << endl;

        cout<<"mapa player 1"<< endl;
        MostrarP1Fim();//mostra resultado final do jogo,se acabou antes mostra a posição dos barcos restantes
        cout<<endl;
        cout<<"mapa player 2"<< endl;
        MostrarP2Fim();

        system("pause");//espera a interação do jogador para encerrar o programa

    arquivo.close();//fecha o arquivo

    return 0;
}

