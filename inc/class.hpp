#ifndef CLASS_HPP_INCLUDED
#define CLASS_HPP_INCLUDED

#include "../inc/mapa.hpp"

using namespace std;

class embarc//classe "progenitora"
{
public:
    void imprimir();//declaração da função para impressão de dados
    int get_linha();//métodos acessores do atributo linha
    void set_linha(int l);
    int get_coluna();//métodos acessores do atributo coluna
    void set_coluna(int cl);
    string get_nome();//métodos acessores do atributo tipo
    void set_nome(string nm);
    string get_onde();//métodos acessores do atributo onde
    void set_ond(string on);
    void posicionar_CanoaP1();//método para posicionar os objetos canoa do Player 1
    void posicionar_SubmarinoP1();//método para posicionar os objetos subamrino do Player 1
    void posicionar_PortaAvioesP1();//método para posicionar os objetos porta_avioes do Player 1
    void posicionar_CanoaP2();//método para posicionar os objetos canoa do Player 2
    void posicionar_SubmarinoP2();//método para posicionar os objetos submarino do Player 2
    void posicionar_PortaAvioesP2();//método para posicionar os objetos porta_avioes do Player 2

private:
    int linha;//atributo linha
    int coluna;//atributo coluna
    string tipo;//atributo tipo
    string onde;//atributo onde(referente para onde o restante da embarcação deve se posicionar)
};

int embarc::get_linha()
{
    return linha;
}

void embarc::set_linha(int l)
{
    linha = l;
}

int embarc::get_coluna()
{
    return coluna;
}

void embarc::set_coluna(int cl)
{
    coluna = cl;
}

string embarc::get_nome()
{
    return tipo;
}
void embarc::set_nome(string nm)
{
    tipo = nm;
}
string embarc::get_onde()
{
    return onde;
}

void embarc::set_ond(string on)
{
    onde = on;
}

class submarino:public embarc//cria a classe submarino que herda atributos e métodos de ambarc
{
public:
    submarino(int L, int CL, string NM, string ON);//delimita parametros para submarino
};

submarino::submarino(int L, int CL, string NM, string ON)
{
    set_linha(L);
    set_coluna(CL);
    set_nome(NM);
    set_ond(ON);
}

class canoa:public embarc//cria a classe canoa que herda atributos e métodos de ambarc
{
public:
    canoa(int L, int CL, string NM, string ON);//delimita parametros para canoa
};

canoa::canoa (int L, int CL, string NM, string ON)
{
    set_linha(L);
    set_coluna(CL);
    set_nome(NM);
    set_ond(ON);
}

class porta_avioes:public embarc//cria a classe porta_avioes que herda atributos e métodos de ambarc
{
public:
    porta_avioes(int L, int CL, string NM, string ON);//delimita parametros
};

porta_avioes::porta_avioes (int L, int CL, string NM, string ON)
{
    set_linha(L);
    set_coluna(CL);
    set_nome(NM);
    set_ond(ON);
}

void embarc::imprimir()
{
    cout<<"Tipo.............: "<<get_nome()<<endl;
    cout<<"Linha............: "<<get_linha()<<endl;
    cout<<"Coluna...........: "<<get_coluna()<<endl;
    cout<<"Localizacao......: "<<get_onde()<< endl;
    cout<<"..................."<< endl;
}

void embarc::posicionar_CanoaP1()
{

                int C;
                int L;

                L = get_linha();
                C = get_coluna();

                if(M1[L][C] != 'c' && M1[L][C] != 's' && M1[L][C] != 'p')//verifica se o espaço está vazio
                {
                        M1[L][C] = 'c';//troca o valor por um char
                }

}

void embarc::posicionar_SubmarinoP1()
{

                int C;
                int L;
                string O;

                L = get_linha();
                C = get_coluna();
                O = get_onde();

                if(O == "cima")//verifica a direção que o objeto delimita
                {
                    if(M1[L][C] != 'c' && M1[L][C] != 's' && M1[L][C] != 'p')
                    {
                        M1[L][C] = 's';
                        M1[L-1][C] = 's';
                    }
                }

                else if(O == "direita")
                {
                    if(M1[L][C] != 'c' && M1[L][C] != 's' && M1[L][C] != 'p')
                    {
                        M1[L][C] = 's';
                        M1[L][C+1] = 's';
                    }
                }

                else if(O == "esquerda")
                {
                    if(M1[L][C] != 'c' && M1[L][C] != 's' && M1[L][C] != 'p')
                    {
                        M1[L][C] = 's';
                        M1[L][C-1] = 's';
                    }
                }

}

void embarc::posicionar_PortaAvioesP1()
{

                int C;
                int L;
                string O;

                L = get_linha();
                C = get_coluna();
                O = get_onde();

                if(O == "cima")
                {
                    if(M1[L][C] != 'c' && M1[L][C] != 's' && M1[L][C] != 'p')
                    {
                        M1[L][C] = 'p';
                        M1[L-1][C] = 'p';
                        M1[L-2][C] = 'p';
                        M1[L-3][C] = 'p';

                    }
                }

                else if(O == "direita")
                {
                    if(M1[L][C] != 'c' && M1[L][C] != 's' && M1[L][C] != 'p')
                    {
                        M1[L][C] = 'p';
                        M1[L][C+1] = 'p';
                        M1[L][C+2] = 'p';
                        M1[L][C+3] = 'p';
                    }
                }

                else if(O == "esquerda")
                {
                    if(M1[L][C] != 'c' && M1[L][C] != 's' && M1[L][C] != 'p')
                    {
                        M1[L][C] = 'p';
                        M1[L][C-1] = 'p';
                        M1[L][C-2] = 'p';
                        M1[L][C-3] = 'p';
                    }
                }

}

void embarc::posicionar_CanoaP2()
{

                int C;
                int L;

                L = get_linha();
                C = get_coluna();

                if(M2[L][C] != 'c' && M2[L][C] != 's' && M2[L][C] != 'p')
                {
                        M2[L][C] = 'c';
                }

}

void embarc::posicionar_SubmarinoP2()
{

                int C;
                int L;
                string O;

                L = get_linha();
                C = get_coluna();
                O = get_onde();

                if(O == "cima")
                {
                    if(M2[L][C] != 'c' && M2[L][C] != 's' && M2[L][C] != 'p')
                    {
                        M2[L][C] = 's';
                        M2[L-1][C] = 's';
                    }
                }

                else if(O == "direita")
                {
                    if(M2[L][C] != 'c' && M2[L][C] != 's' && M2[L][C] != 'p')
                    {
                        M2[L][C] = 's';
                        M2[L][C+1] = 's';
                    }
                }

                else if(O == "esquerda")
                {
                    if(M2[L][C] != 'c' && M2[L][C] != 's' && M2[L][C] != 'p')
                    {
                        M2[L][C] = 's';
                        M2[L][C-1] = 's';
                    }
                }


}

void embarc::posicionar_PortaAvioesP2()
{

                int C;
                int L;
                string O;

                L = get_linha();
                C = get_coluna();
                O = get_onde();

                if(O == "cima")
                {
                    if(M2[L][C] != 'c' && M2[L][C] != 's' && M2[L][C] != 'p')
                    {
                        M2[L][C] = 'p';
                        M2[L-1][C] = 'p';
                        M2[L-2][C] = 'p';
                        M2[L-3][C] = 'p';

                    }
                }

                else if(O == "direita")
                {
                    if(M2[L][C] != 'c' && M2[L][C] != 's' && M2[L][C] != 'p')
                    {
                        M2[L][C] = 'p';
                        M2[L][C+1] = 'p';
                        M2[L][C+2] = 'p';
                        M2[L][C+3] = 'p';
                    }
                }

                else if(O == "esquerda")
                {
                    if(M2[L][C] != 'c' && M2[L][C] != 's' && M2[L][C] != 'p')
                    {
                        M2[L][C] = 'p';
                        M2[L][C-1] = 'p';
                        M2[L][C-2] = 'p';
                        M2[L][C-3] = 'p';
                    }
                }

}


#endif // CLASS_HPP_INCLUDED
