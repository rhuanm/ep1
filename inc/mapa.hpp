#ifndef MAPA_HPP_INCLUDED
#define MAPA_HPP_INCLUDED

#include <iostream>
#include <cstdlib>
#include <ctime>
#include "../inc/class.hpp"

using namespace std;

const int linhas = 13;//determina o inteiro linhas como um valor constante
const int colunas = 13;//determina o inteiro colunas como um valor constante

char M1[linhas][colunas];//cria uma matriz de char para o player 1
char M2[linhas][colunas];//cria uma matriz de char para o player 2

void Limpar()
{
        for(int L=0; L < linhas; L++)
        {
                for(int C=0; C < colunas; C++)//coloca todos os valores das matrizes como 0
                {
                        M1[L][C] = '0';
                        M2[L][C] = '0';
                }
        }
}

void MostrarP1()
{
        for(int L=0; L < linhas; L++)
        {
                for(int C=0; C < colunas; C++)
                {
                        if(M1[L][C] == 'c'||M1[L][C] == 's'||M1[L][C] == 'o'||M1[L][C] == 'p')//verifica se o espaço está vazio
                        {
                            cout << "0 ";
                        }
                        else//mostra espaços vazios e embarcações destruidas
                        {
                            cout << M1[L][C] << " ";
                        }

                }
                cout << endl;
        }

}

void MostrarP2()
{
        for(int L=0; L < linhas; L++)
        {
                for(int C=0; C < colunas; C++)
                {
                        if(M2[L][C] == 'c'||M2[L][C] == 's'||M2[L][C] == 'o'||M2[L][C] == 'p')
                        {
                            cout << "0 ";
                        }
                        else
                        {
                            cout << M2[L][C] << " ";
                        }
                }
                cout << endl;
        }

}

void MostrarP1Fim()
{
        for(int L=0; L < linhas; L++)
        {
                for(int C=0; C < colunas; C++)//mostra todos os valores da matriz
                {
                        cout << M1[L][C] << " ";
                }
                cout << endl;
        }

}

void MostrarP2Fim()
{
        for(int L=0; L < linhas; L++)
        {
                for(int C=0; C < colunas; C++)
                {
                        cout << M2[L][C] << " ";
                }
                cout << endl;
        }

}

int NumeroDeBarcosP1()
{
        int C1=0, S1=0, P1=0, R1;

        for(int L=0; L < linhas; L++)//procura por elementos da matriz
        {
                for(int C=0; C < colunas; C++)
                {
                        if(M1[L][C] == 'c')
                        {
                            C1++;
                        }
                        else if(M1[L][C] == 's'||M1[L][C] == 'o')
                        {
                            S1++;
                        }
                        else if(M1[L][C] == 'p')
                        {
                            P1++;
                        }

                }
        }

        R1=C1+(S1/2)+(P1/4);

        return R1;//retorna o numero de barcos
}

int NumeroDeBarcosP2()
{
        int C2=0, S2=0, P2=0, R2;

        for(int L=0; L < linhas; L++)
        {
                for(int C=0; C < colunas; C++)
                {
                        if(M2[L][C] == 'c')
                        {
                            C2++;
                        }
                        else if(M2[L][C] == 's'||M2[L][C] == 'o')
                        {
                            S2++;
                        }
                        else if(M2[L][C] == 'p')
                        {
                            P2++;
                        }

                }
        }

        R2=C2+(S2/2)+(P2/4);

        return R2;
}

bool AtaqueP1(int Lin,int Col)
{
        srand(42);//função para auxiliar na aleatóriedade do ataque
        int RAND1 = rand()%100;//cria uma variável randomica

        if(M1[Lin][Col] == 'c')//verifica se o espaço está ocupado e substitui ele
        {
            M1[Lin][Col] = 'x';
            return true;
        }

        else if(M1[Lin][Col] == 's')//caso submarino seja atigido substituir o valores por O, refente à perca de vida do submarino
        {
          if(M1[Lin][Col+1] == 's')
          {
            M1[Lin][Col] = 'o';
            M1[Lin][Col+1] = 'o';
            return true;
          }

          else if(M1[Lin-1][Col] == 's')
          {
            M1[Lin][Col] = 'o';
            M1[Lin-1][Col] = 'o';
            return true;
          }

          else if(M1[Lin][Col-1] == 's')
          {
            M1[Lin][Col] = 'o';
            M1[Lin][Col-1] = 'o';
            return true;
          }
        }

        else if(M1[Lin][Col] == 'o')
        {
          if(M1[Lin][Col+1] == 'o')
          {
            M1[Lin][Col] = 'x';
            M1[Lin][Col+1] = 'x';
            return true;
          }

          else if(M1[Lin-1][Col] == 'o')
          {
            M1[Lin][Col] = 'x';
             M1[Lin-1][Col] = 'x';
            return true;
          }

          else if(M1[Lin][Col-1] == 'o')
          {
            M1[Lin][Col] = 'x';
             M1[Lin][Col-1] = 'x';
            return true;
          }
        }

        else if (M1[Lin][Col] == 'p')
        {
          if(RAND1<50)//verifica o valor aleatório, se ele for menor do que 50, o jogador errou
          {
              return false;
          }
          else
          {
            if(M1[Lin][Col+1] == 'p' && M1[Lin][Col+2] == 'p' && M1[Lin][Col+2] == 'p')
            {
                M1[Lin][Col] = 'x';
                M1[Lin][Col+1] = 'x';
                M1[Lin][Col+2] = 'x';
                M1[Lin][Col+3] = 'x';
                return true;
            }

            else if(M1[Lin-1][Col] == 'p' && M1[Lin-2][Col] == 'p' && M1[Lin-3][Col] == 'p')
            {
                M1[Lin][Col] = 'x';
                M1[Lin-1][Col] = 'x';
                M1[Lin-2][Col] = 'x';
                M1[Lin-3][Col] = 'x';
                return true;
            }

            else if(M1[Lin][Col-1] == 'p' && M1[Lin][Col-2] == 'p' && M1[Lin][Col-3] == 'p')
            {
                M1[Lin][Col] = 'x';
                M1[Lin][Col-1] = 'x';
                M1[Lin][Col-2] = 'x';
                M1[Lin][Col-2] = 'x';
                return true;
            }
          }

        }

        else//caso nenhuma condição seja atendida, retorna false, que significa erro
        {
            return false;
        }
}

bool AtaqueP2(int Lin,int Col)
{
        srand(88);
        int RAND1 = rand()%100;

        if(M2[Lin][Col] == 'c')
        {
            M2[Lin][Col] = 'x';
            return true;
        }

        else if(M2[Lin][Col] == 's')
        {
          if(M2[Lin][Col+1] == 's')
          {
            M2[Lin][Col] = 'o';
            M2[Lin][Col+1] = 'o';
            return true;
          }

          else if(M2[Lin-1][Col] == 's')
          {
            M2[Lin][Col] = 'o';
            M2[Lin-1][Col] = 'o';
            return true;
          }

          else if(M2[Lin][Col-1] == 's')
          {
            M2[Lin][Col] = 'o';
            M2[Lin][Col-1] = 'o';
            return true;
          }
        }

        else if(M2[Lin][Col] == 'o')
        {
          if(M2[Lin][Col+1] == 'o')
          {
            M2[Lin][Col] = 'x';
            M2[Lin][Col+1] = 'x';
            return true;
          }

          else if(M2[Lin-1][Col] == 'o')
          {
            M2[Lin][Col] = 'x';
             M2[Lin-1][Col] = 'x';
            return true;
          }

          else if(M2[Lin][Col-1] == 'o')
          {
            M2[Lin][Col] = 'x';
             M2[Lin][Col-1] = 'x';
            return true;
          }
        }

        else if (M2[Lin][Col] == 'p')
        {
          if(RAND1<50)
          {
              return false;
          }
          else
          {
            if(M2[Lin][Col+1] == 'p' && M2[Lin][Col+2] == 'p' && M2[Lin][Col+2] == 'p')
            {
                M2[Lin][Col] = 'x';
                M2[Lin][Col+1] = 'x';
                M2[Lin][Col+2] = 'x';
                M2[Lin][Col+3] = 'x';
                return true;
            }

            else if(M2[Lin-1][Col] == 'p' && M2[Lin-2][Col] == 'p' && M2[Lin-3][Col] == 'p')
            {
                M2[Lin][Col] = 'x';
                M2[Lin-1][Col] = 'x';
                M2[Lin-2][Col] = 'x';
                M2[Lin-3][Col] = 'x';
                return true;
            }

            else if(M2[Lin][Col-1] == 'p' && M2[Lin][Col-2] == 'p' && M2[Lin][Col-3] == 'p')
            {
                M2[Lin][Col] = 'x';
                M2[Lin][Col-1] = 'x';
                M2[Lin][Col-2] = 'x';
                M2[Lin][Col-2] = 'x';
                return true;
            }
          }

        }

        else
        {
            return false;
        }
}

#endif // MAPA_HPP_INCLUDED
